import React, { useRef, useState, useEffect } from 'react'
import { Button, Typography } from '@mui/material';
import './Timer.css'

const Timer = (props) => {
    const Ref = useRef(null);
    const { countDownTimer } = props;

    const [timer, setTimer] = useState(countDownTimer);

    const getTimeRemaining = (e) => {
        const total = Date.parse(e) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / 1000 / 60 / 60) % 24);
        return {
            total, hours, minutes, seconds
        };
    }

    const getSecondsFromString = (timeString) => {
        const [hours, min, seconds] = timeString.split(":")
        return parseInt(seconds) + parseInt(min) * 60 + parseInt(hours) * 60 * 60
    }

    const startTimer = (e) => {
        let { total, hours, minutes, seconds }
            = getTimeRemaining(e);
        if (total >= 0) {
            setTimer(
                (hours > 9 ? hours : '0' + hours) + ':' +
                (minutes > 9 ? minutes : '0' + minutes) + ':'
                + (seconds > 9 ? seconds : '0' + seconds)
            )
        }
    }

    const clearTimer = (e) => {
        setTimer(countDownTimer);
        if (Ref.current) clearInterval(Ref.current);
        const id = setInterval(() => {
            startTimer(e);
        }, 1000)
        Ref.current = id;
    }

    const getDeadTime = () => {
        const deadline = new Date();
        deadline.setSeconds(deadline.getSeconds() + getSecondsFromString(countDownTimer));
        return deadline;
    }

    useEffect(() => {
        clearTimer(getDeadTime());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [countDownTimer]);

    const onClickReset = () => {
        clearTimer(getDeadTime());
    }

    return (
        <div className='timer--container'>
            <Typography variant='h2' className='timer--typography'>{timer}</Typography>
            <Button onClick={() => onClickReset()}>reset</Button>
        </div>
    )
}

export default Timer

import React, { useEffect, useState } from 'react'
import { Box, Paper, Tab, Tabs } from '@mui/material';

import "./App.css"
import Timer from './compnents/Timer/Timer';

const App = () => {
  const [selectedTab, setSelectedTab] = useState(0);
  const [pomodoroProfile, setPomodoroProfile] = useState({
    backGroundColor: "rgb(173, 80, 77)",
    cardColor: "rgb(181, 98, 95)",
    time: "00:25:00",
  });
  const [shortBreakProfile, setShortBreakProfile] = useState({
    backGroundColor: "rgb(77,131,137)",
    cardColor: "rgb(95, 144,149)",
    time: "00:5:00"
  });
  const [longBreakProfile, setLongBreakProfile] = useState({
    backGroundColor: "rgb(57,112,151)",
    cardColor: "rgb(77,127,162)",
    time: "00:15:00"
  });
  const [selectedProfile, setSelectedProfile] = useState(pomodoroProfile);

  const handleChangeTab = (event, newValue) => {
    setSelectedTab(newValue);
    switch (newValue) {
      case 0:
        setSelectedProfile(pomodoroProfile)
        break;
      case 1:
        setSelectedProfile(shortBreakProfile)
        break;
      case 2:
        setSelectedProfile(longBreakProfile)
        break;
      default:
        setSelectedProfile(pomodoroProfile)
        break;
    }
  };

  useEffect(()=>{
    document.body.style.backgroundColor = selectedProfile.backGroundColor
  },[selectedProfile])

  return (
    <Box
      className='container'
    >
      <Paper className='timerCard' sx={{backgroundColor: selectedProfile.cardColor}}>
        <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
          <Tabs value={selectedTab} onChange={handleChangeTab} centered sx={{backgroundColor: selectedProfile.cardColor}}>
            <Tab label="Pomodoro" />
            <Tab label="Short Break" />
            <Tab label="Long Break" />
          </Tabs>
        </Box>
        <Timer countDownTimer={selectedProfile.time}/>
      </Paper>
    </Box>
  )
}

export default App
